# Rapport - Projet Math pour Big Data

>## 1. Descriptif du sujet et de ses finalités

>>### Évolution de la température en France (3.2.2)

On voudrait étudier l'évolution de la température atmosphérique sur le territoire français.
<br>Le problème est que les données historiques de Météo France sont payantes (200000 €/an).
<br>On se contentera donc d’utiliser les quelques données publiques qu’on trouve ici pour arriver à nos fins.

1)  Choisissez 5 villes et considérez les variables statistiques température minimale 12h et température maximale par rapport à ces villes.

2)  Découpez les données en deux pa2010 à 2017 et 2018rties : avant 2018 et courant 2018. 
<br>Utilisez les données de 2018 pour vérifier s’il y a eu un changement significatif des valeurs de température par rapport au passé.

3)  Répétez les tests précédents en découpant les données autrement : avant 2015 et courant 2018. 
<br>Que remarquez vous?

>## 2 Description des données utilisées

Le jeu de données utilisé est au format CSV et encodé en UTF-8 au lien suivant : https://public.opendatasoft.com/explore/dataset/donnees-synop-essentielles-omm/export/?sort=date. 

L'identifiant du jeu de données est "donnees-synop-essentielles-omm" et 2010 à 2017 et 2018isponibles (de 2010 à 2019 inclus).
2010 à 2017 et 2018
2010 à 2017 et 2018ées :
2010 à 2017 et 2018
Données d'observations issues des messages internationaux d’observation en surface (SYNOP) circulant sur le système mondial de télécommunication (SMT) de l’Organisation Météorologique Mondiale (OMM).

Paramètres atmosphériques mesurés (température, humidité, direction et force du vent,pression atmosphérique, hauteur de précipitations) ou observés (temps sensible, description des nuages, visibilité) depuis la surface terrestre.

Selon instrumentation et spécificités locales, d'autres paramètres peuvent être disponibles (hauteur de neige, état du sol, etc.)

>>### 2.2 Colonnes pertinentes du jeu de données 

- "**Date**" *(colonne n°2)* : 
<br>date de l'enregistrement, de type datetime yyyy-MM-dd'T'HH:mm:ss'Z' *(ex : 2018-08-08T11:00:00+02:00)*.

- "**Température minimale sur 12 heures**" *(colonne n°25)* : 
<br>température minimale du matin de l'enregistrement en Kelvin, de type texte.

- "**Température maximale sur 24 heures**" *(colonne n°28)* :
<br>température maximale de toute la journée de l'enregistrement en Kelvin, de type texte.

- "**Nom**" *(colonne n°61)* : 
<br>nom de la ville de l'enregistrement, de type texte.

>>### 2.3 Période du jeu de données

Le présent jeu de données possède les enregistrements à partir de l'année 2010 jusqu'à aujourd'hui (2019).
<br>Toutes les années entre 2010 et 2019 inclus sont enregistrées.

>## 3 Méthodologie suivie

>>### 3.1 Pré-traitement des données

>>>#### 3.1.1 Échantillon de villes

Les 5 villes choisies pour l'analyse sont :

- "NICE"
- "LILLE-LESQUIN"
- "NANTES-BOUGUENAIS"
- "BOURGES"
- "DIJON-LONGVIC"

Le choix de ces 5 villes permet de mieux représenter l'ensemble du territoire français, <br>**les 5 villes étant localisées chacune dans une zone principale différente et qui lui est propre**(dans l'ordre : SUD, NORD, OUEST, CENTRE, EST).
<br>Le choix de la ville de NICE est quand à lui motivé par la proximité de sa localisation et l'Université de Nice-Sophia Antipolis. 

*Remarque : Paris, capitale de la France, est absente dans le jeu de données, comme d'autres grandes villes, notre choix de ville est donc évidemment  limité à celles fournies dans le jeu de données.*

>>>#### 3.1.2 "Allègement" du jeu de données contenant uniquement les villes choisies

À partir du jeu de données original, les enregistrements des 5 villes choisies sont filtrés et enregistrés dans un nouveau jeu de données "allégé".
<br>Le but étant d'éviter de re-parcourir et re-calculer l'ensemble du jeu de données original dans lequel désormais un faible taux d'enregistrements est utile (8.7%).

>>>#### 3.1.3 Sélection des colonnes voulues

Comme l'étape précédente, le principe est d'alléger le jeu de données contenant originellement 64 colonnes, en ne gardant que les colonnes pertinantes définies dans la section 2.2 :
- "Date",
- "Température minimale sur 12 heures",
- "Température maximale sur 24 heures",
- "Nom".

*Remarque : dans notre échantillonage, aucune donnée "Température maximale sur 24 heures" n'est référencée.
<br> De ce fait, nous ne prennon que les données "Température minimale sur 12 heures" pour mener à bien  .*

>>>#### 3.1.4 Découpage du jeu de données désormais "allégé" pour chaque plage annuelle

Puisque l'objectif est de vérifier des changements de températures,
- entre 2018 et avant 2018
- entre 2018 et avant 2015

un découpage pour les plages d'années [2010;2014],[2010;2017] sont effectuées, ainsi que les enregistrements de l'année [2018] seulement.

>>>#### 3.1.5 Isolement des données pour chaque ville pour chaque plage d'années

Les enregistrements désormais ségmentés sont triés dans un dataframe lui correspondant en fonction de sa ville et de sa plage d'années.
<br>Puisque l'échantillon est composé de 5 villes dans lequels 3 plages d'années sont examinées, 15 dataframes sont déclarés permettant dans à l'étape suivante "Statistiques et vérifications d'hypothèses" d'appliquer facilement et de façon distinte les calculs statistiques.

>>### 3.2 Statistiques et hypothèses

Pour chaque ville et chaque année entre 2010 et 2018, la moyenne des températures minimales sur 12h est calculée.
<br>L'assignation de la valeur booléenne TRUE dans le paramètre na.rm de la fonction mean() permet d'ignorer les données vides. 

>>### 3.3 Génération des graphiques

Afin de répondre au sujet, deux graphiques sont générés :
- le premier affichera les données de 2010 à 2017 et 2018
- le deuxième affichera les données de 2010 à 2015 et 2018

Pour ces deux graphiques :

La légende  sera composé de :
- L'axe X (abscisse) représente une dimension temporelle exprimée en années depuis J-C.
- L'axe Y (ordonnée) représente la moyenne de température minimale exprimée en degrès 
<br>*(conversion de Kelvin à degrés Celsius selon la formule mathématique degrés ℃ = Kelvin - 273.15)*

Une courbe d'évolution est générée individuellement pour chaque ville avec un code couleur afin de bien les différencier :

- Bourges : noir
- Nice : rouge
- Lille : vert
- Nantes : bleu foncé
- Dijeu : bleu clair

>>### 3.4 Rédaction du rapport final

Ce présent rapport final est rédigé de manière à permettre de comprendre de manière simple et précise la démarche effectuée pour mener ce projet à bien, à vérifier la méthodologie mise en oeuvre et de mettre à porter toutes les informations utiles.
<br>Chaque étape du projet est donc expliquée et documentée au fur et à mesure de la l'intégration de celui-ci.

>## 4 Résultats obtenus

Voici les deux graphiques générés :

![](https://image.noelshack.com/fichiers/2019/02/3/1547043809-image2.png "Graphique n°1 de températures (de 2010 à 2017 et 2018)")
*Graphique n°1 de températures (de 2010 à 2017 et 2018)*

![](http://image.noelshack.com/fichiers/2019/02/3/1547043809-image2.png "Graphique n°2 de températures (de 2010 à 2015 et 2018)")
*Graphique n°2 de températures (de 2010 à 2015 et 2018*

D'une manière générale, une légère hausse de température peut être constatée pour l'ensemble des villes entre 2011 et 2018.

Des épiphénomènes peuvent être remarqués pour l'année 2011 et 2014 : il s'agit d'années où la canicule à sévit en Europe de l'ouest.

Les 5 villes mesurées suivent toutes la même tendance (hausse/baisse) de température, il en ressort donc que la variation de température tend vers la même proportionnalité pour chacune des villes.

>## 5 Présentation de groupe et rôles dans le projet

Ce groupe n°8 est composé de 3 étudiants alternants :

- Florian DEMOULIN :
<br>Programmation de la partie "statistiques et vérifications d'hypothèses" et "génération des graphiques"

- Anthony GOMEZ :
<br>Programmation d'une partie de la partie "pre-traitement"
<br>Rédaction du rapport final
<br>Rédaction du support de présentation

- Cédric ORTEGA :
<br>Programmation de la partie "pre-traitement" et "statistiques et vérifications d'hypothèses"