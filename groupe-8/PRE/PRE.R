# Lecture du fichier csv 

data <- read.csv("groupe-8/DATA/data.csv", sep = ";", encoding = "UTF-8")

# "Allègement" du jeu de données contenant uniquement les villes choisies

dataVilles <- subset(data, data$Nom == "NICE" | data$Nom == "LILLE-LESQUIN"| data$Nom == "BOURGES"| data$Nom == "NANTES-BOUGUENAIS"| data$Nom == "DIJON-LONGVIC")

#  Sélection des colonnes voulues

dataVillesColumns <- subset(dataVilles, select=c("Date","Température.minimale.sur.12.heures","Température.maximale.sur.24.heures","Nom"))

# Découpage du jeu de données pour chaque plages annuelles

library(stringr)
annee <- str_split_fixed(dataVillesColumns$Date, "-", 2)
dim(annee) 
anneeNumeric <- as.numeric(annee[,1])

dataAnneeAvant2018 <- subset(dataVillesColumns, anneeNumeric < 2018) # Entre 2010 et 2017 inclus
dataAnneeAvant2015 <- subset(dataVillesColumns, anneeNumeric < 2015) # Entre 2010 et 2014 inclus
dataAnnee2018 <- subset(dataVillesColumns, anneeNumeric == 2018) # 2018

# Isolement des données pour chaque ville pour chaque plage d'années

LILLE.dataAnneeAvant2018 = subset(dataAnneeAvant2018, dataAnneeAvant2018$Nom == "LILLE-LESQUIN")
LILLE.dataAnneeAvant2015 = subset(dataAnneeAvant2015, dataAnneeAvant2015$Nom == "LILLE-LESQUIN")
LILLE.dataAnnee2018 = subset(dataAnnee2018, dataAnnee2018$Nom == "LILLE-LESQUIN")

NICE.dataAnneeAvant2018 = subset(dataAnneeAvant2018, dataAnneeAvant2018$Nom == "NICE")
NICE.dataAnneeAvant2015 = subset(dataAnneeAvant2015, dataAnneeAvant2015$Nom == "NICE")
NICE.dataAnnee2018 = subset(dataAnnee2018, dataAnnee2018$Nom == "NICE")

NANTES.dataAnneeAvant2018 = subset(dataAnneeAvant2018, dataAnneeAvant2018$Nom == "NANTES-BOUGUENAIS")
NANTES.dataAnneeAvant2015 = subset(dataAnneeAvant2015, dataAnneeAvant2015$Nom == "NANTES-BOUGUENAIS")
NANTES.dataAnnee2018 = subset(dataAnnee2018, dataAnnee2018$Nom == "NANTES-BOUGUENAIS")

BOURGES.dataAnneeAvant2018 = subset(dataAnneeAvant2018, dataAnneeAvant2018$Nom == "BOURGES")
BOURGES.dataAnneeAvant2015 = subset(dataAnneeAvant2015, dataAnneeAvant2015$Nom == "BOURGES")
BOURGES.dataAnnee2018 = subset(dataAnnee2018, dataAnnee2018$Nom == "BOURGES")

DIJON.dataAnneeAvant2018 = subset(dataAnneeAvant2018, dataAnneeAvant2018$Nom == "DIJON-LONGVIC")
DIJON.dataAnneeAvant2015 = subset(dataAnneeAvant2015, dataAnneeAvant2015$Nom == "DIJON-LONGVIC")
DIJON.dataAnnee2018 = subset(dataAnnee2018, dataAnnee2018$Nom == "DIJON-LONGVIC")

